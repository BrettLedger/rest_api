from django.core.management.base import BaseCommand
from rest_api.models import ShoeColor, ShoeType

class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'adding shoe colors and types to the database'

    def _create_shoe_color(self):
        color1 = ShoeColor(color_name='Red')
        color1.save()

        color2 = ShoeColor(color_name='Orange')
        color2.save()

        color3 = ShoeColor(color_name='Yellow')
        color3.save()

        color4 = ShoeColor(color_name='Green')
        color4.save()

        color5 = ShoeColor(color_name='Blue')
        color5.save()

        color6 = ShoeColor(color_name='Indigo')
        color6.save()

        color7 = ShoeColor(color_name='Violet')
        color7.save()

        color8 = ShoeColor(color_name='White')
        color8.save()

        color9 = ShoeColor(color_name='Black')
        color9.save()

    def _create_shoe_type(self):
        type1 = ShoeType(style='sneaker')
        type1.save()

        type2 = ShoeType(style='boot')
        type2.save()

        type3 = ShoeType(style='sandal')
        type3.save()

        type4 = ShoeType(style='dress')
        type4.save()

        type5 = ShoeType(style='other')
        type5.save()

    def handle(self, *args, **options):
        self._create_shoe_color()
        self._create_shoe_type()